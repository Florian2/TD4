#!/usr/bin/python3
#  -*- coding:Utf-8 -*-

import requests
from bs4 import BeautifulSoup



def question_4():
    UNI = requests.post("http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav",data={"submit-form":'','catalog':"catalogue-2015-2016",'title':"LP","degree":"DP","place":"45000"})
    soup = BeautifulSoup(UNI.text,'lxml')
    results = soup.find('div',class_="results")
    formation=[]
    with open('formation.txt','w') as fic:
        for f in results.findAll('li',class_="hit"):
            formation.append(f.text)
            fic.write(str(f.text))
            fic.write('\n')
    return formation

print(question_4())