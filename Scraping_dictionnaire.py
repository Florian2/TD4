#!/usr/bin/python3
#  -*- coding:Utf-8 -*-
import requests
from bs4 import BeautifulSoup


def get_definition(x):

    payload = {'action':'define','dict':'wn','query':x}
    html2 = requests.get('http://services.aonaware.com/DictService/Default.aspx',params=payload).text
    soup = BeautifulSoup(html2,'lxml')
    return soup.find('pre').text

def get_definition_file(file):
    lines=[]
    with open(file,'r') as f:
        for l in f.readlines():
            lines.append(l.replace('\n',''))
    with open('dictionary.txt','w') as f2:
        for word in lines :
            f2.write(get_definition(word))
            f2.write('\n')
    f2.close()

    print(lines)


get_definition_file('vocabulary.txt')